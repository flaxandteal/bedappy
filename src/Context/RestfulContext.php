<?php
namespace Flaxandteal\Bedappy\Context;

use Behat\Behat\Context\Context;
use GuzzleHttp\ClientInterface;
use Flaxandteal\Bedappy\Service\RequestService;
use Flaxandteal\Bedappy\Service\RecallService;

/**
 * Api client aware interface
 *
 * @author Christer Edvartsen <cogo@starzinger.net>
 */
interface RestfulContext extends Context
{
    /**
     * Set the prefix for API routes
     *
     * Unlike the Behat API extension, we do not want an FQDN.
     *
     * @param string $apiPrefix
     * @return self
     */
    function setApiPrefix($apiPrefix);

    /**
     * Set the prefix for RESTful models
     *
     * @param string $modelPrefix
     * @return self
     */
    function setModelPrefix($modelPrefix);

    /**
     * Set the user model for RESTful models
     *
     * @param string $userModel
     * @return self
     */
    function setUserModel($userModel);

    /**
     * Should we use a mock HTTP client for any outgoing requests
     * (at least via GuzzleHttp\Client)
     *
     * @param bool $userModel
     * @return self
     */
    function setMockOutgoingClient($mockOutgoingClient);

    /**
     * Should be overridden to do anything required between
     * configuration being set and initialization of the rest
     * of the system
     *
     * @return void
     */
    function boot();

    /**
     *
     * @param Flaxandteal/Bedappy/Service/RecallService
     * @return voicd
     */
    function setRecallService(RecallService $recallService);

    /**
     *
     * @param Flaxandteal/Bedappy/Service/RequestService
     * @return voicd
     */
    function setRequestService(RequestService $requestService);
}
