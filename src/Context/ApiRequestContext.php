<?php

namespace Flaxandteal\Bedappy\Context;

use App;
use Flaxandteal\Bedappy\Service\RecallService;
use Flaxandteal\Bedappy\Service\RequestService;
use phpseclib\Crypt\RSA;
use Laravel\Passport\Passport;
use Laravel\Sanctum\Sanctum;
use Artisan;
use Auth;
use Carbon\Carbon;
use DB;
use ReflectionException;
use stdClass;
use Cache;
use Hash;
use Str;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\RawMinkContext;
use Laracasts\Behat\Context\DatabaseTransactions;
use PHPUnit_Framework_Assert as PHPUnit;
use Imbo\BehatApiExtension\ArrayContainsComparator;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Behat\Behat\Context\SnippetAcceptingContext;
use Imbo\BehatApiExtension\Context\ArrayContainsComparatorAwareContext;
use Exception;
use Behat\Mink\Mink;
use PHPUnit\Framework\Assert;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Client;

/**
 * Defines application features from the specific context.
 */
class ApiRequestContext extends RawMinkContext implements RestfulContext, ArrayContainsComparatorAwareContext, SnippetAcceptingContext
{
    protected $internalClient = null;

    protected $internalHandler = null;

    protected $mockOutgoingClient = false;

    protected $ownResource = 'me';

    protected $authFramework = 'passport';

    const VALID_AUTH_FRAMEWORKS = [
        'passport',
        'sanctum'
    ];

    /**
     * Instance of the comparator that handles matching of JSON
     *
     * @var ArrayContainsComparator
     */
    protected $arrayContainsComparator;

    /**
     * Swap ⌚ tokens for times
     *
     * @param string $string
     */
    public function replaceTimes($string)
    {
        $string = preg_replace('/^(DB|JSON)/', '', $string);
        preg_match_all('/⌚([ a-z:\-.A-Z0-9]+)/', $string, $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            $term = trim($match[1]);
            $date = Carbon::parse($term);
            $string = str_replace($match[0], $date->toDateTimeString(), $string);
        }

        return $string;
    }

    public function setRecallService(RecallService $recallService)
    {
        $this->recallService = $recallService;
    }

    public function setRequestService(RequestService $requestService)
    {
        $this->requestService = $requestService;
    }

    /**
     * {@inheritdoc}
     */
    public function setArrayContainsComparator(ArrayContainsComparator $comparator) {
        $this->arrayContainsComparator = $comparator;

        return $this;
    }

    /**
     * Set the auth framework
     *
     * @param string $authFramework
     * @return self
     */
    function setAuthFramework($authFramework)
    {
        $this->authFramework = $authFramework;

        return $this;
    }

    /**
     * Set the User model
     *
     * @param string $userModel
     * @return self
     */
    function setUserModel($userModel)
    {
        $this->userModel = $userModel;

        return $this;
    }

    /**
     * Set the type of casing for models in URL building
     *
     * @param string $urlCasing
     * @return self
     */
    function setUrlCasing($urlCasing)
    {
        $this->urlCasing = $urlCasing;

        return $urlCasing;
    }

    /**
     * Should outgoing HTTP requests be mocked
     *
     * This works by swapping the DI GuzzleHttp\Client for one with a MockHandler
     *
     * @param bool $mockOutgoingClient
     * @return self
     */
    function setMockOutgoingClient($mockOutgoingClient)
    {
        $this->mockOutgoingClient = $mockOutgoingClient;

        return $this;
    }

    /**
     * Set the prefix for API routes
     *
     * Unlike the Behat API extension, we do not want an FQDN.
     *
     * @param string $apiPrefix
     * @return self
     */
    function setApiPrefix($apiPrefix)
    {
        $this->apiPrefix = $apiPrefix;

        return $this;
    }

    /**
     * Set the prefix for RESTful models
     *
     * @param string $modelPrefix
     * @return self
     */
    function setModelPrefix($modelPrefix)
    {
        $this->modelPrefix = $modelPrefix;

        return $this;
    }

    /**
     * While a state machine like this wouldn't be ideal in the code itself,
     * here it provides a useful flow for the natural-language-like
     * syntax of Gherkin
     */
    protected $_response = null;

    protected $_token = null;

    protected $_parameters = [];

    public function getResponse()
    {
        return $this->_response;
    }

    /**
     * @When /^I set parameter "([^"]*)" to the ID of this (.*)$/
     */
    public function iSetParameterToTheID($arg1, $arg2)
    {
        $this->_parameters[$arg1] = $this->recallService->getKnownId($arg2);
    }

    /**
     * @Given the resource for myself is at '/:arg1'
     */
    function theResourceForMyselfIsAt(PyStringNode $string)
    {
        $this->ownResource = $string;
    }

    /**
     * @Given I want to get my own properties through the API
     */
    public function iWantToGetMyOwnPropertiesThroughTheApi()
    {
        $this->_url = $this->ownResource;
        $this->recallService->setActiveObject(null);

        $this->_method = 'get';
        $this->_isForm = false;
        $this->_sendObject = false;
    }

    public function toUrlCasing($value) {
        switch ($this->urlCasing) {
            case 'kebab':
                return Str::kebab($value);
            case 'camel':
                return Str::camel($value);
            case 'snake':
                return Str::snake($value);
        }
    }

    /**
     * @Given /^I want to ([\w-]+) (a|the|this) ([\w]*) of this (.*) through the API$/
     */
    public function iWantToTheOfThisThroughTheAPI($arg1, $prep, $arg2, $arg3)
    {
        $type = Str::camel($arg3);
        $this->_url = Str::plural($this->toUrlCasing($type)) . '/' . $this->recallService->getKnownId($arg3);
        $this->recallService->setActiveObject(null);

        switch ($arg1) {
        case 'update':
            $this->_method = 'put';
            $this->_isForm = true;
            $this->_sendObject = false;
            $this->_url .= '/' . Str::plural($this->toUrlCasing($arg2)) . '/' . $this->recallService->getKnownId($arg2);
            break;
        case 'destroy':
            $this->_method = 'delete';
            $this->_isForm = false;
            $this->_sendObject = false;
            $this->_url .= '/' . Str::plural($this->toUrlCasing($arg2)) . '/' . $this->recallService->getKnownId($arg2);
            break;
        case 'show':
            $this->_method = 'get';
            $this->_isForm = false;
            $this->_sendObject = false;
            $this->_url .= '/' . Str::plural($this->toUrlCasing($arg2)) . '/' . $this->recallService->getKnownId($arg2);
            break;
        case 'bulk-update':
            $this->_method = 'put';
            $this->_isForm = true;
            $this->_sendObject = true;
            $this->_url .= '/' . Str::plural($this->toUrlCasing($arg2));
            break;
        case 'store':
            $this->_method = 'post';
            $this->_isForm = true;
            $this->_sendObject = false;
            $this->_url .= '/' . Str::plural($this->toUrlCasing($arg2));
            break;
        case 'list':
            $this->_method = 'get';
            $this->_isForm = false;
            $this->_sendObject = false;
            $this->_url .= '/' . Str::plural($this->toUrlCasing($arg2));
            break;
        default:
            throw new Exception("Unknown API verb for specific submodel request");
        }
    }

    /**
     * @Given /^I want to ([\w]+) this (.*) through the API$/
     */
    public function iWantToThisThroughTheAPI($arg1, $arg2)
    {
        $type = Str::camel($arg2);
        $this->_url = Str::plural($this->toUrlCasing($type)) . '/' . $this->recallService->getKnownId($arg2);
        $this->recallService->setActiveObject(null);

        switch ($arg1) {
        case 'update':
            $this->_method = 'put';
            $this->_isForm = true;
            $this->_sendObject = false;
            break;
        case 'destroy':
            $this->_method = 'delete';
            $this->_isForm = false;
            $this->_sendObject = false;
            break;
        case 'show':
            $this->_method = 'get';
            $this->_isForm = false;
            $this->_sendObject = false;
            break;
        default:
            throw new Exception("Unknown API verb for specific ('this') request");
        }
    }

    /**
     * @Given /^I want to ([\w-]+) ?a?n? ([A-Z].*) through the API$/
     */
    public function iWantToAThroughTheAPI($arg1, $arg2)
    {
        $this->_url = Str::camel(Str::plural($arg2));
        $this->recallService->setActiveObject(null);

        switch ($arg1) {
            case 'bulk-update':
                $this->_method = 'put';
                $this->_isForm = true;
                $this->_sendObject = true;
                break;
            case 'store':
                $this->_method = 'post';
                $this->_isForm = true;
                $this->_sendObject = false;
                break;
            case 'show':
                $this->_method = 'get';
                $this->_isForm = false;
                $this->_sendObject = false;
                break;
            case 'list':
                $this->_method = 'get';
                $this->_isForm = false;
                $this->_sendObject = false;
                break;
            default:
                throw new Exception("Unknown API verb for unspecific ('a') request");
        }
    }

    /**
     * @When I send a request with query:
     */
    public function iSendARequestWithQuery(PyStringNode $string)
    {
        $string = $this->replaceTimes($string);
        $string = $this->recallService->replaceKnownIds($string);

        $query = json_decode((string)$string);

        $this->iSendARequest($query);
    }

    /**
     * @When I send a request
     */
    public function iSendARequest($query = null)
    {
        $targetUrl = url($this->apiPrefix . '/' . $this->_url);

        if ($query) {
            // Very basic for current purposes
            // TODO: tidy up for general usage
            $targetUrl .= '?';
            $targetParameters = [];
            foreach ($query as $key => $value) {
                $targetParameters[] = e($key) . '=' . e($value);
            }
            $targetUrl .= implode('&', $targetParameters);
        }

        $fields = [];
        $content = null;
        if ($this->recallService->getActiveObject()) {
            if ($this->_isForm) {
                $fields = (array)$this->recallService->getActiveObject();
            }
            if ($this->_sendObject) {
                $content = json_encode($this->recallService->getActiveObject());
            }
        }
        print_r("[sending " . $this->_method . " to {$targetUrl}]");

        $fields = array_merge($this->_parameters, $fields);

        $this->requestService->setDriver($this->getSession()->getDriver());
        $this->_response = $this->requestService->request(
            $this->_method,
            $targetUrl,
            $fields,
            $this->_isForm,
            $content
        );
    }

    /**
     * @Given I have an API token
     */
    public function iHaveAnApiToken()
    {
        $client = $this->getSession()
            ->getDriver()
            ->getClient();
        $request = $client->request('GET', '/');
        csrf_token();
    }

    /**
     * Begin a new suite.
     *
     * @BeforeSuite
     */
    public static function beginSuite()
    {
        self::makeOauthKeys();
    }

    /**
     * Begin a database transaction.
     *
     * @BeforeScenario
     */
    public static function beginTransaction()
    {
        $migrator = app('migrator');
        if (! $migrator->repositoryExists()) {
            app('migration.repository')->createRepository();
        }
        $paths = array_merge($migrator->paths(), [app()->databasePath().DIRECTORY_SEPARATOR.'migrations']);
        $migrator->run($paths);

        DB::beginTransaction();

        app('DatabaseSeeder')->run();

        $permissionRegistrar = App::make(\Spatie\Permission\PermissionRegistrar::class);
        $permissionRegistrar->registerPermissions();
    }

    public static function makeOauthKeys()
    {
        if (class_exists(RSA::class) && class_exists(Passport::class)) {
            $rsa = new RSA;
            $keys = $rsa->createKey(4096);

            file_put_contents(Passport::keyPath('oauth-public.key'), array_get($keys, 'publickey'));
            file_put_contents(Passport::keyPath('oauth-private.key'), array_get($keys, 'privatekey'));
        }
    }

    /**
     *
     * Roll it back after the scenario.
     *
     * @AfterScenario
     */
    public static function rollback()
    {
        DB::rollback();
        Cache::flush();
    }

    public function boot()
    {
    }

    /**
     * @Then I can do something with Laravel
     */
    public function iCanDoSomethingWithLaravel()
    {
        PHPUnit::assertEquals('.env.behat', app()->environmentFile());
        PHPUnit::assertEquals('acceptance', env('APP_ENV'));
        PHPUnit::assertTrue(config('app.debug'));
    }

    /**
     * @Given I am logged out
     */
    public function iAmLoggedOut()
    {
        Auth::guard('web')->logout();
        Auth::guard('api')->setUser(null);
    }

    /**
     * @Given I am logged in as :arg1
     */
    public function iAmLoggedInAs($arg1)
    {
        $user = $this->thereIsAUser($arg1);
        $this->login($user);
        return $user;
    }

    public function login($user)
    {
        switch ($this->authFramework) {
            case 'passport':
                Passport::actingAs($user);
                break;
            case 'sanctum':
                Sanctum::actingAs($user);
                break;
        }
        Auth::guard('web')->login($user);
        return $user;
    }

    /**
     * @Given /^I am logged in as "([^"]*)", who is an? (.*)$/
     */
    public function iAmLoggedInAsWhoIsA($arg1, $arg2)
    {
        $user = $this->thereIsAUserWhoIsA($arg1, $arg2);
        $this->login($user);
        return $user;
    }

    /**
     * @When I post a reset form to :arg1 with content:
     *
     * DEPRECATED
     */
    public function iPostAResetFormToWithContent($arg1, PyStringNode $string)
    {
        return $this->iPostAFormToWithContent($arg1, $string);
    }

    /**
     * @When I post a form to :arg1 with content:
     */
    public function iPostAFormToWithContent($arg1, PyStringNode $string)
    {
        $string = $this->replaceTimes($string);
        $string = $this->recallService->replaceKnownIds($string);

        $parameters = json_decode($string, true);

        $this->requestService->setDriver($this->getSession()->getDriver());
        $this->_response = $this->requestService->request(
            'post',
            $arg1,
            $parameters,
            true
        );
    }

    /**
     * @When :arg1 requests a reset link
     */
    public function requestsAResetLink($arg1)
    {
        $user = App::make($this->userModel);
        $user->whereEmail($arg1)->firstOrFail();
        $broker = App::make('auth.password');
        $this->_token = $broker->createToken($user);
    }

    /**
     * @When :arg1 follows a reset link
     */
    public function followsAResetLink($arg1)
    {
        $this->requestsAResetLink($arg1);
        $this->visitPath('password/reset/' . $this->_token);
    }
}
