<?php

namespace Flaxandteal\Bedappy\Context;

use App;
use Notification;
use Mail;
use Flaxandteal\Bedappy\Service\RecallService;
use phpseclib\Crypt\RSA;
use Laravel\Passport\Passport;
use Artisan;
use Auth;
use Carbon\Carbon;
use DB;
use ReflectionException;
use stdClass;
use Cache;
use Hash;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\RawMinkContext;
use Laracasts\Behat\Context\DatabaseTransactions;
use PHPUnit_Framework_Assert as PHPUnit;
use Imbo\BehatApiExtension\ArrayContainsComparator;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Behat\Behat\Context\SnippetAcceptingContext;
use Imbo\BehatApiExtension\Context\ArrayContainsComparatorAwareContext;
use Exception;
use Behat\Mink\Mink;
use PHPUnit\Framework\Assert;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Client;

/**
 * Defines application features from the specific context.
 */
class ApiContext extends ApiRequestContext
{
    use ResponseContextTrait;
    use ModelContextTrait;

    public function boot()
    {
        Notification::fake();
        Mail::fake();
    }
}
