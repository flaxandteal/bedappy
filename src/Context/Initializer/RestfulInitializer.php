<?php
namespace Flaxandteal\Bedappy\Context\Initializer;

use Flaxandteal\Bedappy\Context\RestfulContext;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\Initializer\ContextInitializer;

/**
 * RESTful initializer
 *
 * Initializer for feature contexts that implement the RestfulAware interface.
 *
 * @author Phil Weir <phil.weir@flaxandteal.co.uk>
 */
class RestfulInitializer implements ContextInitializer {
    /**
     * @var string
     */
    private $modelPrefix;

    /**
     * @var string
     */
    private $urlCasing;

    /**
     * @var string
     */
    private $authFramework;

    /**
     * @var array
     */
    private $authDefaultUserDetails;

    /**
     * @var object
     */
    private $requestService;

    /**
     * @var object
     */
    private $recallService;

    /**
     * Class constructor
     *
     * @param string $baseUri
     */
    public function __construct($apiPrefix, $modelPrefix, $userModel,
            $urlCasing, $mockOutgoingClient,
            $authDefaultUserDetails, $authFramework,
            $recallService, $requestService) {
        $this->apiPrefix = $apiPrefix;
        $this->modelPrefix = $modelPrefix;
        $this->userModel = $userModel;
        $this->urlCasing = $urlCasing;
        $this->mockOutgoingClient = $mockOutgoingClient;
        $this->recallService = $recallService;
        $this->requestService = $requestService;
        $this->authDefaultUserDetails = $authDefaultUserDetails;
        $this->authFramework = $authFramework;
    }

    /**
     * Initialize the context
     *
     * Inject the Guzzle client if the context implements the ApiClientAwareContext interface
     *
     * @param Context $context
     */
    public function initializeContext(Context $context) {
        if ($context instanceof RestfulContext) {
            $context->setApiPrefix($this->apiPrefix);
            $context->setModelPrefix($this->modelPrefix);
            $context->setUserModel($this->userModel);
            $context->setUrlCasing($this->urlCasing);
            $context->setAuthFramework($this->authFramework);
            $context->setMockOutgoingClient(true);
            $context->setDefaultAuthUserDetails($this->authDefaultUserDetails);
            $context->setRecallService($this->recallService);
            $context->setRequestService($this->requestService);

            $context->boot();
        }
    }
}
