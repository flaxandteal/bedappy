<?php

namespace Flaxandteal\Bedappy\Context;

use App;
use Flaxandteal\Bedappy\Service\RecallService;
use phpseclib\Crypt\RSA;
use Laravel\Passport\Passport;
use Artisan;
use Auth;
use Carbon\Carbon;
use DB;
use ReflectionException;
use stdClass;
use Cache;
use Hash;
use Str;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\RawMinkContext;
use Laracasts\Behat\Context\DatabaseTransactions;
use PHPUnit_Framework_Assert as PHPUnit;
use Imbo\BehatApiExtension\ArrayContainsComparator;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Behat\Behat\Context\SnippetAcceptingContext;
use Imbo\BehatApiExtension\Context\ArrayContainsComparatorAwareContext;
use Exception;
use Behat\Mink\Mink;
use PHPUnit\Framework\Assert;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Client;

/**
 * Defines application features from the specific context.
 */
trait ModelContextTrait
{
    protected $_pointInTime = null;

    /**
     * Set the default user details for authenticating users.
     *
     * @param array $defaultUserDetails
     * @return self
     */
    function setDefaultAuthUserDetails($defaultUserDetails)
    {
        $this->defaultUserDetails = $defaultUserDetails;

        return $this;
    }

    /**
     * Swap ⌚ tokens for times
     *
     * @param string $string
     */
    public function replaceTimes($string)
    {
        $string = preg_replace('/^(DB|JSON)/', '', $string);
        preg_match_all('/⌚([ a-z:\-.A-Z0-9]+)/', $string, $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {
            $term = trim($match[1]);
            $date = Carbon::parse($term);
            $string = str_replace($match[0], $date->toDateTimeString(), $string);
        }

        return $string;
    }

    /**
     * @Given /^the current time is (.*)$/
     */
    public function theCurrentTimeIs($arg1)
    {
        Carbon::setTestNow(Carbon::parse($arg1));
    }

    /**
     * @Given /^this (.*)'s ([^,]*) already has known ID and these properties:$/
     */
    public function thisWhichHasKnownIdAlreadyHasProperties($arg1, $arg2, PyStringNode $string)
    {
        $obj = $this->thisAlreadyHasProperties($arg1, $arg2, $string);

        $this->recallService->addKnownId($arg2, $obj);
    }

    /**
     * @Given /^this (.*) already has these properties:$/
     */
    public function thisAlreadyHasTheseProperties($arg1, PyStringNode $string)
    {
        $obj = $this->recallService->getLastAlready($arg1);

        $string = $this->replaceTimes($string);
        $string = $this->recallService->replaceKnownIds($string);

        $fields = json_decode($string, true);
        if (!$fields) {
            throw new Exception("Could not convert from JSON: " . $string);
        }
        $obj->forceFill($fields);
        $obj->save();

        $this->recallService->setLastAlready($arg1, $obj);

        return $obj;
    }

    /**
     * @Given /^this (.*)'s (.*) already has properties:$/
     */
    public function thisAlreadyHasProperties($arg1, $arg2, PyStringNode $string)
    {
        $parent = $this->recallService->getLastAlready($arg1);

        $attribute = Str::camel($arg2);

        $string = $this->replaceTimes($string);
        $string = $this->recallService->replaceKnownIds($string);

        $fields = json_decode($string, true);
        if (!$fields) {
            throw new Exception("Could not convert from JSON: " . $string);
        }
        $obj = $parent->{$attribute};
        $obj->forceFill($fields);

        $parent->{$attribute}->save();

        $this->recallService->setLastAlready($arg2, $obj);

        return $obj;
    }

    /**
     * @Given /^I already have an? ([^,]*), with known ID:$/
     */
    public function iAlreadyHaveAWithKnownID($arg1, PyStringNode $string)
    {
        $obj = $this->iAlreadyHaveA($arg1, $string);

        $this->recallService->addKnownId($arg1, $obj);

        $this->recallService->setLastAlready($arg1, $obj);
    }

    /**
     * @Given its properties will be:
     */
    public function itsPropertiesWillBe(PyStringNode $string)
    {
        $string = $this->replaceTimes($string);
        $string = $this->recallService->replaceKnownIds($string);

        $this->recallService->setActiveObject(json_decode((string)$string, true));

        if ($this->recallService->getActiveObject() === null) {
            abort(
                1,
                "The property object did not contain valid JSON " .
                "(check any known IDs are, for example, capitalized for " .
                "models and small for roles):\n" . (string)$string
            );
        }
    }

    /**
     * @Given /^it will have a "([^"]*)" property for this (.*)$/
     */
    public function itWillHaveAPropertyForThis($arg1, $arg2)
    {
        if (!$this->recallService->getActiveObject()) {
            $this->recallService->setActiveObject(new \stdClass);
        }
        $obj = $this->recallService->getActiveObject();
        $obj->{$arg1} = $this->recallService->getKnownId($arg2);
        $this->recallService->setActiveObject($obj);
    }

    /**
     * @Given /^this (.*) already has this (.*) as a (.*)$/
     */
    public function thisAlreadyHasThisAsA($arg1, $arg2, $arg3)
    {
        $obj = $this->recallService->getKnown($arg1);
        $related = $this->recallService->getKnown($arg2);
        $obj->{$arg3}()->attach($related);
    }

    /**
     * @Given /^I have this (.*) as one of my (.*)$/
     */
    public function iHaveThisAsOneOfMy($arg1, $arg2)
    {
        $user = Auth::user();
        $related = $this->recallService->getKnown($arg1);
        $user->{$arg2}()->attach($related);
    }

    /**
     * @Given /^my "([^"]*)" becomes (.*)$/
     */
    public function myBecomes($arg1, $arg2)
    {
        $user = Auth::user();
        if ($arg2 == "this point in time") {
            $this->_pointInTime = Carbon::now();
            $value = $this->_pointInTime->format('Y-m-d H:i:s');
        } elseif ($arg2 == "ten seconds ago") {
            $this->_pointInTime = Carbon::now()->subSeconds(10);
            $value = $this->_pointInTime->format('Y-m-d H:i:s');
        } else {
            $value = json_decode($arg2);
        }
        $user->{$arg1} = $value;
        $user->save();
    }

    /**
     * @Then /^my "([^"]*)" is (.*)$/
     */
    public function myIs($arg1, $arg2)
    {
        if ($arg2 == "that point in time") {
            $value = $this->_pointInTime->format('Y-m-d H:i:s');
        } else {
            $value = json_decode($arg2);
        }
        Assert::assertEquals(Auth::user()->{$arg1}, $value, Auth::user()->{$arg1});
    }

    /**
     * @Then /^my "([^"]*)" isn't (.*)$/
     */
    public function myIsnt($arg1, $arg2)
    {
        if ($arg2 == "that point in time") {
            $value = $this->_pointInTime->format('Y-m-d H:i:s');
        } else {
            $value = json_decode($arg2);
        }
        Assert::assertNotEquals(Auth::user()->{$arg1}, $value, Auth::user()->{$arg1});
    }

    /**
     * @Given the user :arg1
     */
    public function theUser($arg1)
    {
        $user = App::make($this->userModel);
        $user->create([
            'email' => $arg1,
            'name' => "Known Name",
            'password' => Hash::make('password')
        ]);
    }

    /**
     * @Given there is a user :arg1
     */
    public function thereIsAUser($arg1)
    {
        $user = App::make($this->userModel);
        $existing = $user->whereEmail($arg1)->first();

        if ($existing) {
            $user = $existing;
        } else {
            $userDetails = array_merge([
                'email' => $arg1,
                'password' => Hash::make('password')
            ], $this->defaultUserDetails);
            $user->fill($userDetails);
            $user->save();
        }

        $this->recallService->addKnownId('User', $user);
        return $user;
    }

    /**
     * @Given /^there is a user "([^"]*)", who is an? (.*)$/
     */
    public function thereIsAUserWhoIsA($arg1, $arg2)
    {
        $user = $this->thereIsAUser($arg1);
        $roles = explode(' ', $arg2);
        if (property_exists($user, 'hasRole')) {
            foreach ($roles as $role) {
                if (! $user->hasRole($role)) {
                    $user = $user->assignRole($role, 'api');
                }
            }
            $user->load('roles');
        }
        foreach ($roles as $role) {
            $this->recallService->addKnownId($role, $user);
        }
        return $user;
    }

    /**
     * @Given /^I already have an? ([^,]*):$/
     */
    public function iAlreadyHaveA($arg1, PyStringNode $string)
    {
        $obj = App::make($this->modelPrefix .ucfirst(Str::camel($arg1)));

        $string = $this->replaceTimes($string);
        $string = $this->recallService->replaceKnownIds($string);

        $fields = json_decode($string, true);
        if (!$fields) {
            throw new Exception("Could not convert from JSON: " . $string);
        }
        $obj->forceFill($fields);
        $obj->save();

        $this->recallService->setLastAlready($arg1, $obj);

        return $obj;
    }
}
