<?php

namespace Flaxandteal\Bedappy\Context;

use Ds\Queue;
use App;
use phpseclib\Crypt\RSA;
use Laravel\Passport\Passport;
use Artisan;
use Auth;
use Carbon\Carbon;
use DB;
use ReflectionException;
use stdClass;
use Cache;
use Hash;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\RawMinkContext;
use Laracasts\Behat\Context\DatabaseTransactions;
use PHPUnit_Framework_Assert as PHPUnit;
use Imbo\BehatApiExtension\ArrayContainsComparator;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Behat\Behat\Context\SnippetAcceptingContext;
use Imbo\BehatApiExtension\Context\ArrayContainsComparatorAwareContext;
use Exception;
use Behat\Mink\Mink;
use PHPUnit\Framework\Assert;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Client;
use Flaxandteal\Bedappy\Service\RecallService;
use Flaxandteal\Bedappy\Service\RequestService;

/**
 * Defines application features from the specific context.
 */
class InternalClientContext extends RawMinkContext implements RestfulContext, SnippetAcceptingContext
{
    protected $internalClient = null;

    protected $internalHandler = null;

    protected $mockOutgoingClient = false;

    protected $queue = [];

    public function setUserModel($userModel)
    {
        $this->userModel = $userModel;

        return $this;
    }

    /**
     * Set the prefix for API routes
     *
     * Unlike the Behat API extension, we do not want an FQDN.
     *
     * @param string $apiPrefix
     * @return self
     */
    function setApiPrefix($apiPrefix)
    {
        $this->apiPrefix = $apiPrefix;

        return $this;
    }

    /**
     * Set the prefix for RESTful models
     *
     * @param string $modelPrefix
     * @return self
     */
    function setModelPrefix($modelPrefix)
    {
        $this->modelPrefix = $modelPrefix;

        return $this;
    }

    public function setRecallService(RecallService $recallService)
    {
        $this->recallService = $recallService;
    }

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function boot()
    {
        $this->queue = [];

        if ($this->mockOutgoingClient) {
            $this->serverSideHttpRequestsGetDummyResponses();
        } else {
            $this->internalClient = app()->make(Client::class);
        }

        app()->bind(
            Client::class,
            function () {
                return $this->internalClient;
            }
        );
    }

    /**
     * Should outgoing HTTP requests be mocked
     *
     * This works by swapping the DI GuzzleHttp\Client for one with a MockHandler
     *
     * @param bool $mockOutgoingClient
     * @return self
     */
    function setMockOutgoingClient($mockOutgoingClient)
    {
        $this->mockOutgoingClient = $mockOutgoingClient;

        return $this;
    }

    /**
     * @Given /^server-side HTTP requests get dummy responses:?$/
     */
    function serverSideHttpRequestsGetDummyResponses(PyStringNode $string = null)
    {
        $client = app()->make(Client::class);
        $string = $this->recallService->replaceKnownIds($string ?: "{}");

        if ($client->getConfig('handler') instanceof BottomlessMockHandler) {
            $this->internalHandler = $client->getConfig('handler');
            $this->internalClient = $client;
        } else {
            $this->internalHandler = new BottomlessMockHandler([]);
            $this->internalClient = new Client(["handler" => $this->internalHandler]);
            app()->instance(Client::class, $this->internalClient);
        }

        $body = json_decode((string)$string);
        $this->internalHandler->setDefaultResponse(
            function ($req, $opts) use ($body) {
                return new Response(200, [], json_encode($body), "1.1", null);
            }
        );
    }

    /**
     * @Given we add to the dummy HTTP response queue:
     */
    function weAddToTheDummyHttpResponseQueue(PyStringNode $string)
    {
        $string = $this->recallService->replaceKnownIds($string);

        $responseData = json_decode((string)$string);
        if (! is_array($responseData)) {
            $responseData = [$responseData];
        }

        $queue = array_map(
            function ($response) {
                return new Response(200, [], json_encode($response), "1.1", null);
            },
            $responseData
        );

        foreach ($queue as $closure) {
            $this->internalHandler->append($closure);
        }
    }

    public function setRequestService(RequestService $requestService)
    {
        $this->requestService = $requestService;
    }
}
