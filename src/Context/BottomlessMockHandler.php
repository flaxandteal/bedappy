<?php

namespace Flaxandteal\Bedappy\Context;

use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Handler\MockHandler;

class BottomlessMockHandler extends MockHandler
{
    protected $defaultResponse;

    public function setDefaultResponse($defaultResponse)
    {
        $this->defaultResponse = $defaultResponse;
    }

    public function __invoke(RequestInterface $request, array $options)
    {
        if (! $this->count()) {
            $this->append($this->defaultResponse);
        }

        return parent::__invoke($request, $options);
    }
}

