<?php

namespace Flaxandteal\Bedappy\Context;

use App;
use Flaxandteal\Bedappy\Service\RecallService;
use phpseclib\Crypt\RSA;
use Laravel\Passport\Passport;
use Artisan;
use Auth;
use Carbon\Carbon;
use DB;
use ReflectionException;
use stdClass;
use Cache;
use Hash;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\RawMinkContext;
use Laracasts\Behat\Context\DatabaseTransactions;
use PHPUnit_Framework_Assert as PHPUnit;
use Imbo\BehatApiExtension\ArrayContainsComparator;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Behat\Behat\Context\SnippetAcceptingContext;
use Imbo\BehatApiExtension\Context\ArrayContainsComparatorAwareContext;
use Exception;
use Behat\Mink\Mink;
use PHPUnit\Framework\Assert;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Client;

/**
 * Defines application features from the specific context.
 */
trait ResponseContextTrait
{
    /**
     * @Given the response should be an array
     */
    public function theResponseShouldBeAnArray()
    {
        Assert::assertTrue(is_array($this->getResponse()['data']));
    }

    /**
     * @Given /^the response should be an array of length (\d*)$/
     */
    public function theResponseShouldBeAnArrayOfLength($arg1)
    {
        $this->theResponseShouldBeAnArray();

        Assert::assertEquals($arg1, count($this->getResponse()['data']));
    }

    /**
     * @Then /^I note that "([^"]*)" from the last response is the ID of an? (\w*)$/
     */
    public function iNoteThatFromTheLastResponseIsTheIdOfA($arg1, $arg2)
    {
        try {
            $obj = App::make($this->modelPrefix . ucfirst(camel_case($arg2)));
        } catch (ReflectionException $e) {
            $obj = (object)['id' => null];
        }
        $content = $this->getResponse()['data'];

        $obj->id = $content->{$arg1};

        $this->recallService->addKnownId($arg2, $obj);

        $this->recallService->setLastAlready($arg2, $obj);
    }

    /**
     * @Then /^the response should(.*) match JSON:$/
     */
    public function theResponseShouldMatchJSON($arg1, PyStringNode $string)
    {
        return $this->containsJson($arg1, $string, false);
    }

    /**
     * @Then /^the response should(.*) contain JSON:$/
     */
    public function theResponseShouldContainJSON($arg1, PyStringNode $string)
    {
        return $this->containsJson($arg1, $string, true);
    }

    public function containsJson($arg1, PyStringNode $string, bool $searchArray)
    {
        $this->theResponseShouldBeJSON();

        $string = $this->replaceTimes($string);
        $string = $this->recallService->replaceKnownIds($string);

        $shouldContain = json_decode((string)$string, true);

        if ($shouldContain === null) {
            throw new Exception("JSON string is not valid:\n$string");
        }

        // We use this contrast to allow the natural JSON interpretation
        // _and_ the more easily introspectable array version for checking
        // against
        $content = $this->getResponse()['data'];
        $contentAsArray = $this->getResponse()['content'];
        if (array_key_exists('data', $contentAsArray)) {
            $contentAsArray = $contentAsArray['data'];
        }

        if (is_array($content) && $searchArray) {
            $found = false;
            foreach ($contentAsArray as $entry) {
                $found |= empty($this->arrayDiff($shouldContain, $entry));
            }
            $missingItems = $found ? [] : $shouldContain;
        } else {
            $missingItems = $this->arrayDiff($shouldContain, $contentAsArray);
        }

        if (trim($arg1) == 'not') {
            Assert::assertNotEmpty($missingItems, "Response matching: " . json_encode($content));
        } else {
            Assert::assertEmpty($missingItems, "Response is not matching: " . json_encode($missingItems, JSON_PRETTY_PRINT) . "\n" . json_encode($content, JSON_PRETTY_PRINT));
        }
    }

    /**
     * @Then /^the response should not have an? "([^"]*)" property$/
     */
    public function theResponseShouldNotHaveAProperty($arg1)
    {
        if (is_array($this->getResponse()['data'])) {
            foreach ($this->getResponse()['data'] as $entry) {
                $entry = json_decode(json_encode($entry));
                Assert::assertObjectNotHasAttribute($arg1, $entry);
            }
        } else {
            Assert::assertObjectNotHasAttribute($arg1, $this->getResponse()['data']);
        }
    }

    /**
     * @Then /^the response should have an? "([^"]*)" property$/
     */
    public function theResponseShouldHaveAProperty($arg1)
    {
        if (is_array($this->getResponse()['data'])) {
            foreach ($this->getResponse()['data'] as $entry) {
                Assert::assertObjectHasAttribute($arg1, $entry);
            }
        } else {
            Assert::assertObjectHasAttribute($arg1, $this->getResponse()['data']);
        }
    }

    protected static function arrayDiff($arr1, $arr2)
    {
        $missingArrays = [];
        $shouldContainWithoutNested = [];
        foreach ($arr1 as $key => $value) {
            if (is_array($value)) {
                if (!array_key_exists($key, $arr2)) {
                    $missingArrays[$key] = [$value, null];
                } else if (!is_array($arr2[$key])) {
                    $missingArrays[$key] = [$value, $arr2[$key]];
                } else {
                    $missingNestedArrays = self::arrayDiff($value, $arr2[$key]);
                    if ($missingNestedArrays) {
                        $missingArrays[$key] = $missingNestedArrays;
                    }
                }
            } else {
                $shouldContainWithoutNested[$key] = $value;
            }
        }
        $missingItems = array_diff_assoc($shouldContainWithoutNested, $arr2);

        return array_merge($missingItems, $missingArrays);
    }

    /**
     * @Then /^the response should have an? "([^"]*)" property, which is the ID of this (.*)$/
     */
    public function theResponseShouldHaveAPropertyWhichIsThisIdOfThis($arg1, $arg2)
    {
        $this->theResponseShouldHaveAProperty($arg1);

        Assert::assertEquals($this->getResponse()['data']->{$arg1}, $this->recallService->getKnownId($arg2));
    }

    /**
     * @Then /^the response should have an? "([^"]*)" property, which is an? (.*)$/
     */
    public function theResponseShouldHaveAPropertyWhichIsA($arg1, $arg2)
    {
        $this->theResponseShouldHaveAProperty($arg1);

        $value = $this->getResponse()['data']->{$arg1};

        switch ($arg2) {
        case 'number':
            Assert::assertTrue(is_numeric($value));
            break;
        case 'uuid':
            // TODO: tighten this up for general applicability
            Assert::assertRegExp('/^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$/', $value);
            break;
        default:
            throw new Exception("Do not know how to test for {$arg2}");
        }
    }

    /**
     * @Then the response should be forbidden
     */
    public function theResponseShouldBeForbidden()
    {
        if ($this->getResponse()['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->getResponse()['content']);
        } else {
            $message = "Response was not as expected";
        }

        Assert::assertEquals(
            403,
            $this->getResponse()['status'],
            $message
        );
    }

    /**
     * @Then the response should be redirected
     */
    public function theResponseShouldBeRedirected()
    {
        if ($this->getResponse()['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->getResponse()['content']);
        } else {
            $message = "Response was not as expected";
        }

        Assert::assertEquals(
            302,
            $this->getResponse()['status'],
            $message
        );
    }

    /**
     * @Then the response should be unprocessable because :arg1 has problem :arg2
     */
    public function theResponseShouldBeUnprocessableBecause($arg1, $arg2)
    {
        $this->theResponseShouldBeUnprocessable();

        Assert::assertEquals(implode(', ', $this->getResponse()['content'][$arg1]), $arg2);
    }

    /**
     * @Then the response should be unprocessable
     */
    public function theResponseShouldBeUnprocessable()
    {
        Assert::assertEquals(
            422,
            $this->getResponse()['status'],
            "Response was not as expected"
        );
    }

    /**
     * @Then the response should be unauthorized
     */
    public function theResponseShouldBeUnauthorized()
    {
        if ($this->getResponse()['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->getResponse()['content']);
        } else {
            $message = "Response was not as expected";
        }

        Assert::assertEquals(
            401,
            $this->getResponse()['status'],
            $message
        );
    }

    /**
     * @Then the response should be successful creation
     */
    public function theResponseShouldBeSuccessfulCreation()
    {
        if ($this->getResponse()['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->getResponse()['content']);
        } else {
            $message = "Request did not indicate successful creation";
        }

        Assert::assertEquals(
            201,
            $this->getResponse()['status'],
            $message
        );
    }

    /**
     * @Then the response should be successful
     */
    public function theResponseShouldBeSuccessful()
    {
        if ($this->getResponse()['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->getResponse()['content']);
        } else {
            $message = "Request was not successful";
        }

        Assert::assertEquals(
            200,
            $this->getResponse()['status'],
            $message
        );
    }

    /**
     * @Then the response should be missing
     */
    public function theResponseShouldBeMissing()
    {
        if ($this->getResponse()['status'] == 422) {
            $message = "Unprocessable: " . json_encode($this->getResponse()['content']);
        } else {
            $message = "Response was not as expected";
        }

        Assert::assertEquals(
            404,
            $this->getResponse()['status'],
            $message
        );
    }

    /**
     * @Then the response should be JSON
     */
    public function theResponseShouldBeJSON()
    {
        Assert::assertEquals(
            'application/json',
            $this->getResponse()['type']
        );
    }

    /**
     * @Then the response should be redirected to :arg1
     */
    public function theResponseShouldBeRedirectedTo($arg1)
    {
        $driver = $this->getSession()
            ->getDriver();

        Assert::assertEquals(
            302,
            $driver->getStatusCode()
        );

        Assert::assertEquals(
            $driver->getResponseHeaders()['location'][0],
            $arg1
        );
    }
}
