<?php

namespace Flaxandteal\Bedappy\Service;

class RecallService
{
    const RECALL_SERVICE_ID = 'bedappy.recall_service';

    protected $_known = [];

    protected $_knownId = [];

    protected $_knownIds = [];

    protected $_lastAlready = [];

    protected $_activeObject = null;

    public function setActiveObject($activeObject)
    {
        $this->_activeObject = $activeObject;
    }

    public function getActiveObject()
    {
        return $this->_activeObject;
    }

    public function setLastAlready($arg, $obj)
    {
        $this->_lastAlready[$arg] = $obj;
    }

    public function getLastAlready($arg)
    {
        return $this->_lastAlready[$arg];
    }

    public function getKnown($arg)
    {
        return $this->_known[$arg];
    }

    public function getKnownId($arg)
    {
        return $this->_knownId[$arg];
    }

    public function addKnownId($name, $obj)
    {
        $this->_knownId[$name] = $obj->id;
        $this->_known[$name] = $obj;
        if (!array_key_exists($name, $this->_knownIds)) {
            $this->_knownIds[$name] = [];
        }
        $this->_knownIds[$name][] = $obj->id;
    }

    /**
     * Swap KNOWN_ID tokens for known IDs
     *
     * @param string $string
     */
    public function replaceKnownIds($string)
    {
        $string = preg_replace('/^(DB|JSON)/', '', $string);
        foreach ($this->_knownIds as $type => $arr) {
            foreach ($arr as $ix => $id) {
                $id = is_numeric($id) ? $id : '"' . $id . '"';
                $string = str_replace('{KNOWN_ID:' . $type . ':' . ($ix + 1) . '}', $id, $string);
                $string = preg_replace('/🛂 ' . $type . ' ' . ($ix + 1) . '/', $id, $string);
            }
        }
        foreach ($this->_knownId as $type => $id) {
            $id = is_numeric($id) ? $id : '"' . $id . '"';
            $string = str_replace('{KNOWN_ID:' . $type . '}', $id, $string);
            $string = preg_replace('/🛂 ' . $type . '/', $id, $string);
        }

        return $string;
    }
}
