<?php

namespace Flaxandteal\Bedappy\Service;

class RequestService
{
    const REQUEST_SERVICE_ID = 'bedappy.request_service';

    protected $_driver;

    protected $_request;

    protected $_token;

    protected $responseComparisonKey = null;

    /**
     * Set the key, if any, that should be extracted from a response before comparing,
     * such as "data" or null if the supplied JSON should be compared with the whole
     * response object.
     *
     * @param $responseComparisonKey
     */
    public function __construct($responseComparisonKey = null) {
        $this->responseComparisonKey = $responseComparisonKey;
    }

    public function setCsrfToken($token)
    {
        $this->_token = $token;
    }

    public function setDriver($driver)
    {
        $this->_driver = $driver;
    }

    public function request($method, $uri, $parameters, $form=false, $content=null)
    {
        $driver = $this->_driver;
        $client = $driver
            ->getClient();
        $client->followRedirects(false);

        $contentType = 'application/json';

        if ($form) {
            $contentType = 'application/x-www-form-urlencoded';
        }

        $headers = [
            'CONTENT_TYPE' => $contentType,
            'HTTP_X_CSRF_TOKEN' => $this->_token,
            'HTTP_X_REQUESTED_WITH' => 'XMLHttpRequest',
            'HTTP_ACCEPT' => 'application/json'
        ];
        $request = $client->request(
            $method,
            $uri,
            $parameters,
            [],
            $headers,
            $content
        );

        $type = 'text/html';
        $responseHeaders = $driver->getResponseHeaders();
        if (array_key_exists('content-type', $responseHeaders)) {
            $type = $responseHeaders['content-type'][0];
            // Strip charset if required
            if (strpos($type, ';')) {
                $type = substr($type, 0, strpos($type, ';'));
            }
        }

        $contentText = $driver->getContent();
        $data = null;
        if ($type == 'application/json') {
            $content = json_decode($contentText, true);
            if ($this->responseComparisonKey) {
                if (isset($content[$this->responseComparisonKey])) {
                    $data = $content[$this->responseComparisonKey];
                }
            } else {
                $data = $content;
            }

            if (count($data) > 1 || array_keys($data) != ['0']) {
                $data = json_decode($contentText);
                if ($this->responseComparisonKey) {
                    $data = $data->{$this->responseComparisonKey};
                }
            }
        } else {
            $content = $contentText;
        }

        $this->_response = [
            'status' => $driver->getStatusCode(),
            'type' => $type,
            'content' => $content,
            'data' => $data
        ];

        return $this->_response;
    }

    function getResponse()
    {
        return $this->_response;
    }

    function get($url)
    {
        $client = $this->_driver
            ->getClient();
        $request = $client->request('GET', '/');
    }
}
