# Bedappy

This package streamlines behavioural API testing with Laravel, and spatie/laravel-permission.

Given auto-seeded roles "admin" and "plebeian", a Todo Eloquent model and
matching `Route::resource` routes, it allows feature tests such as:

```
  Feature: Todo Handling
    In order to manage todos
    As a user
    I want to be able to use the REST API

    Scenario: todo.store
      Given I am logged in as "user@example.com", who is a plebeian
      And I have an API token
      And I want to store a Todo through the API
      And its properties will be:
      """JSON
      {
        "name": "TODO#1",
        "description": "Write that flippin README",
        "owner_id": {KNOWN_ID:User}
      }
      """
      When I send a request
      Then the response should be successful
      And the response should contain JSON:
      """
      {
        "name": "TODO#1",
        "description": "Write that flippin README"
      }
      """

    Scenario: todo.index
      Given I am logged in as "admin@example.com", who is an admin
      And I have an API token
      And I want to list Todos through the API
      When I send a request
      Then the response should be successful
      And the response should contain JSON:
      """
      {
        "data": [
          {
            "type": "todos",
            "id": {KNOWN_ID:Todo},
            "attributes": {
              "name": "TODO#1",
              "description": "Write that flippin README"
            },
            "relations": {
              "owner": {
                "type": "users",
                "id": {KNOWN_ID:plebeian}
              }
            }
          }
        ]
      }
      """
```
