The authors of Bedappy thank Christer Edvartsen for the Behat extension
template used for Bedappy, behat-api-extension.

Bedappy authors:
* Phil Weir <phil.weir@flaxandteal.co.uk>

With thanks to the clients confidential and public, including the Open Data
Institute, who contributed to the development of this project.
